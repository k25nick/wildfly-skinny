FROM k25nick/openjdk-photon:13.0.1
LABEL maintainer="k25nick@gmail.com"

ENV WILDFLY_VERSION 18.0.0.Final
ENV JBOSS_HOME /opt/jboss/wildfly

USER root
WORKDIR /opt/jboss/wildfly

RUN mkdir -p $JBOSS_HOME \
    && mkdir -p /opt/jboss \
    && chmod 755 /opt/jboss \
    && cd $JBOSS_HOME \
    && curl -O https://download.jboss.org/wildfly/$WILDFLY_VERSION/wildfly-$WILDFLY_VERSION.tar.gz \
	&& yum install -y gzip tar \
    && tar xf wildfly-$WILDFLY_VERSION.tar.gz --strip 1 \
	&& yum remove -y gzip tar \
	&& yum clean all \
    && rm wildfly-$WILDFLY_VERSION.tar.gz \
    && rm -rf $JBOSS_HOME/bin/client \
    && rm -rf $JBOSS_HOME/domain \
    && rm -rf $JBOSS_HOME/appclient \
    && rm -rf $JBOSS_HOME/welcome-content/* \
    && rm -rf $JBOSS_HOME/docs \
    && chmod -R g+rw ${JBOSS_HOME}

ENV LAUNCH_JBOSS_IN_BACKGROUND true

EXPOSE 8080 8443

CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0"]
