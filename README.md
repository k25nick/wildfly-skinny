# wildfly-skinny

A skinnier version of the Wildlfy docker images. This version is based on a smaller openjdk image than the official releases and removes unnecessary folders from the Wildfly installation.